package pl.michalrys.prog2.zad3_enum_world_directions;

import org.junit.Test;
import pl.michalrys.prog2.zad3_enum_world_directions.directions.UserControler;

public class ClientTest {
    @Test
    public void shouldNotGiveExceptionWhenPrintDirections() {
        // given
        UserControler userControler = new UserControler();

        // when
        userControler.printDirections();
        // then
        // no exception expected
    }
}