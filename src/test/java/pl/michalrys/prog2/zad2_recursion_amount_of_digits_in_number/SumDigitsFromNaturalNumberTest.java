package pl.michalrys.prog2.zad2_recursion_amount_of_digits_in_number;

import org.junit.Assert;
import org.junit.Test;

public class SumDigitsFromNaturalNumberTest {
    @Test(expected = IllegalValueException.class)
    public void shouldNotCreateObjectForGivenValueOutOfAllowableScope() {
        // given
        int wrongNumber = -10;
        // when
        SumDigitsFromNaturalNumber sumDigits = new SumDigitsFromNaturalNumber(wrongNumber);
        // then
        // throw exception
    }

    @Test
    public void shouldCreateObjectForGivenCorrectValue() {
        // given
        int correctNumber = 148;
        // when
        SumDigitsFromNaturalNumber sumDigits = new SumDigitsFromNaturalNumber(correctNumber);
        // then
        Assert.assertEquals(correctNumber, sumDigits.getNumber());
    }

    @Test
    public void shouldAddDigitsForGivenDecimalValue() {
        // given
        int correctValue = 148;
        SumDigitsFromNaturalNumber sumDigits = new SumDigitsFromNaturalNumber(correctValue);
        // when
        int sum = sumDigits.getSum();
        // then
        Assert.assertEquals(1+4+8, sum);
    }

    @Test
    public void shouldAddDigitsForGivenDecimalValueWhenSimplyCodeMethodWasUsed() {
        // given
        int correctValue = 148;
        SumDigitsFromNaturalNumber sumDigits = new SumDigitsFromNaturalNumber(correctValue);
        // when
        int sum = sumDigits.getSumSimplyCode(0, 148);
        // then
        Assert.assertEquals(1+4+8, sum);
    }
}