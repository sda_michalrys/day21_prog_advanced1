package pl.michalrys.prog2.zad1_recursion_decimal2binary;

import org.junit.Test;

public class NegativeDecimalValueExceptionTest {
    @Test (expected = NegativeDecimalValueException.class)
    public void shouldReturnValuableMessageWhenExceptionIsGiven() {
        // given
        int decimalValue = 10;
        NegativeDecimalValueException exception = new NegativeDecimalValueException(decimalValue);
        String message = exception.getMessage();

        // when
        // then

    }
}