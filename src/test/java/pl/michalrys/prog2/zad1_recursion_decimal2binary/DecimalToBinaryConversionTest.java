package pl.michalrys.prog2.zad1_recursion_decimal2binary;

import org.junit.Assert;
import org.junit.Test;

public class DecimalToBinaryConversionTest {
    @Test
    public void shouldConvertToBinaryGivenDecimalValue() {
        // given
        int decimalValue = 20;
        DecimalToBinaryConversion decimalToBinaryConversion = new DecimalToBinaryConversion();

        // when
        String binaryValue = decimalToBinaryConversion.getBinaryValue(decimalValue);

        // then
        Assert.assertEquals("10100", binaryValue);
    }

    @Test
    public void shouldConvertToBinaryGivenMaxDecimalValue() {
        // given
        int decimalValue = Integer.MAX_VALUE;
        DecimalToBinaryConversion decimalToBinaryConversion = new DecimalToBinaryConversion();

        // when
        String binaryValue = decimalToBinaryConversion.getBinaryValue(decimalValue);

        // then
        Assert.assertEquals("1111111111111111111111111111111", binaryValue);
    }

    @Test (expected = NegativeDecimalValueException.class)
    public void shouldNotConvertToBinaryGivenNegativeDecimalValue() {
        // given
        int decimalValue = -5;
        DecimalToBinaryConversion decimalToBinaryConversion = new DecimalToBinaryConversion();

        // when
        String binaryValue = decimalToBinaryConversion.getBinaryValue(decimalValue);

        // then
    }
}