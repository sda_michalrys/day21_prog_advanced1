package pl.michalrys.prog2.zad6_shop_basket_buy_sth;

import pl.michalrys.prog2.zad6_shop_basket_buy_sth.basket.domain.item_characteristics.Name;
import pl.michalrys.prog2.zad6_shop_basket_buy_sth.basket.persistance_in_memory.BasketController;
import pl.michalrys.prog2.zad6_shop_basket_buy_sth.basket.persistance_in_memory.ProductsController;

public class Client {

    public static void main(String[] args) {
        // we need list of products available in the shop
        ProductsController productsController = new ProductsController();
        productsController.add("Milk Laciate");
        productsController.add("BlackTea Lipton 20");
        productsController.add("Orange Africa 1");
        productsController.add("Orange Africa 2");
        productsController.add("BlackTea Minutka 20");

        // basket controller should know about all available products in shop
        BasketController basketController = new BasketController(productsController.getListOfProducts());

//
        basketController.add("Milk Laciate");
        basketController.add("BlackTea Lipton 20", 5);
        basketController.add("Orange Africa 2", 5);
        basketController.add("Orange Africa 1", 10);
        basketController.remove("BlackTea Lipton 20", 1);
        basketController.remove("BlackTea Lipton 20");

        // below throw exceptions
        basketController.remove("Orange Africa 2", 1);
        //basketController.add("BlackTea Lipton 20", -1);
        //basketController.remove("Orange Africa 1", 5);
        //basketController.add("TyskieBeer500ml");
        //basketController.remove("TyskieBeer500ml");

        System.out.println(basketController.get());
        System.out.println(basketController.getPrice());
    }
}
