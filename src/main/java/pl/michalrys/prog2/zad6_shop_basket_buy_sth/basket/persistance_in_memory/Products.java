package pl.michalrys.prog2.zad6_shop_basket_buy_sth.basket.persistance_in_memory;

import java.util.HashSet;
import java.util.Set;

class Products {
    private Set<String> productsList;

    public Products() {
        this.productsList = new HashSet<>();
    }

    public void add(String productName) {
        productsList.add(productName);
    }

    public Set<String> get() {
        return productsList;
    }
}
