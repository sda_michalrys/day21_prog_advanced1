package pl.michalrys.prog2.zad6_shop_basket_buy_sth.basket.domain.item_characteristics;

import pl.michalrys.prog2.zad6_shop_basket_buy_sth.basket.exceptions.IllegalAmountOfItemException;

public class Amount implements ItemCharacteristic {
    private int howMany;

    public Amount(int howMany) {
        if(howMany <= 0) {
            throw new IllegalAmountOfItemException(howMany);
        }
        this.howMany = howMany;
    }

    public void increase() {
        howMany++;
    }

    public void increase(int manyTimes) {
        if(manyTimes <= 0) {
            throw new IllegalAmountOfItemException(manyTimes);
        }
        for(int i = 1; i <= manyTimes; i++) {
            increase();
        }
    }

    public void decrease() {
        howMany--;
    }

    public void decrease(int manyTimes) {
        if(manyTimes >= howMany) {
            throw new IllegalAmountOfItemException(manyTimes);
        }
        for(int i = 1; i <= manyTimes; i++) {
            decrease();
        }
    }

    public int get() {
        return howMany;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Amount amount = (Amount) o;

        return howMany == amount.howMany;
    }

    @Override
    public int hashCode() {
        return howMany;
    }
}
