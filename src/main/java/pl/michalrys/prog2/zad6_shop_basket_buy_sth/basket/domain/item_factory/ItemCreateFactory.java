package pl.michalrys.prog2.zad6_shop_basket_buy_sth.basket.domain.item_factory;

import pl.michalrys.prog2.zad6_shop_basket_buy_sth.basket.domain.item_characteristics.ItemCharacteristic;
import pl.michalrys.prog2.zad6_shop_basket_buy_sth.basket.domain.item_characteristics.ItemToBuyType;
import pl.michalrys.prog2.zad6_shop_basket_buy_sth.basket.domain.item_characteristics.Name;
import pl.michalrys.prog2.zad6_shop_basket_buy_sth.basket.domain.item_characteristics.Price;

public class ItemCreateFactory implements ItemFactory {

    @Override
    public ItemToBuy create(String itemToAdd) {

        ItemCharacteristic type;
        ItemCharacteristic name;
        Price price;

        switch (itemToAdd) {
            case "Orange Africa 1":
                type = ItemToBuyType.ORANGE;
                name = new Name(itemToAdd);
                price = new Price(1.59);
                return new MilkLaciate(type, name, price);

            case "Orange Africa 2":
                type = ItemToBuyType.ORANGE;
                name = new Name(itemToAdd);
                price = new Price(0.59);
                return new OrangeAfrica2(type, name, price);

            case "BlackTea Lipton 20":
                type = ItemToBuyType.TEA;
                name = new Name(itemToAdd);
                price = new Price(10.11);
                return new BlackTea_Lipton_20(type, name, price);

            case "BlackTea Minutka 20":
                type = ItemToBuyType.TEA;
                name = new Name(itemToAdd);
                price = new Price(8.50);
                return new BlackTeaMinutka20(type, name, price);

            case "Milk Laciate":
                type = ItemToBuyType.MILK;
                name = new Name(itemToAdd);
                price = new Price(2.50);
                return new MilkLaciate(type, name, price);
        }
        return null;
    }
}
