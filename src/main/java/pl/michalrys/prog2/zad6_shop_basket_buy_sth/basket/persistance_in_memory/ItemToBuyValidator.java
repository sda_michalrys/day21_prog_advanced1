package pl.michalrys.prog2.zad6_shop_basket_buy_sth.basket.persistance_in_memory;

import pl.michalrys.prog2.zad6_shop_basket_buy_sth.basket.exceptions.ItemDoNotExistInProductListException;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

class ItemToBuyValidator {
    final private Set<String> productsList;

    public ItemToBuyValidator(Set<String> productsList) {
        this.productsList = productsList;
    }

    public boolean isCorrect(String userInputItemName) {
        // check if given word exists in database
        if(productsList.contains(userInputItemName)) {
            return true;
        }
        throw new ItemDoNotExistInProductListException(userInputItemName);
    }


}
