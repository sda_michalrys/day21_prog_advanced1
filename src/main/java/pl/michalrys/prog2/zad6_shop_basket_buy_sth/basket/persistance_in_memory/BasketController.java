package pl.michalrys.prog2.zad6_shop_basket_buy_sth.basket.persistance_in_memory;

import pl.michalrys.prog2.zad6_shop_basket_buy_sth.basket.domain.item_factory.ItemCreateFactory;
import pl.michalrys.prog2.zad6_shop_basket_buy_sth.basket.exceptions.WrongValueOfAmoutItemsToAddException;

import java.util.Set;

public class BasketController {
    final private Basket basket;
    final private ItemToBuyValidator itemToBuyValidator;
    final private ItemCreateFactory itemCreateFactory;
    final private Set<String> productsList;

    public BasketController(Set<String> products) {
        basket = new Basket();
        productsList = products;
        itemToBuyValidator = new ItemToBuyValidator(productsList);
        itemCreateFactory = new ItemCreateFactory();
    }

    public void add(String item) {
        if (itemToBuyValidator.isCorrect(item)) {
            basket.add(itemCreateFactory.create(item));
        }
    }

    public void add(String item, int howMany) {
        // implemented by recursion
        if(howMany <= 0) {
            throw new WrongValueOfAmoutItemsToAddException(howMany);
        }

        if (itemToBuyValidator.isCorrect(item)) {
            basket.add(itemCreateFactory.create(item));
            howMany--;
        }
        if(howMany != 0) {
            add(item, howMany);
        }
    }

//    public void add(String item, int howMany) {
//        if (itemToBuyValidator.isCorrect(item)) {
//            basket.add(itemCreateFactory.create(item), howMany);
//        }
//    }

    public void remove(String item) {
        if (itemToBuyValidator.isCorrect(item)) {
            basket.remove(itemCreateFactory.create(item));
        }
    }

    public void remove(String item, int howMany) {
        // implemented without recursion
        if (itemToBuyValidator.isCorrect(item)) {
            basket.remove(itemCreateFactory.create(item), howMany);
        }
    }

    public String get() {
        return basket.get();
    }

    public String getPrice() {
        return basket.getPrice();
    }
}
