package pl.michalrys.prog2.zad6_shop_basket_buy_sth.basket.exceptions;

public class ItemDoNotExistInProductListException extends RuntimeException {
    public ItemDoNotExistInProductListException(String userInputItemName) {
        super("Given product name does not exist in product list : " + userInputItemName);
    }
}
