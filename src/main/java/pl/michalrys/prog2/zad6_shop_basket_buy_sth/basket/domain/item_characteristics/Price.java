package pl.michalrys.prog2.zad6_shop_basket_buy_sth.basket.domain.item_characteristics;

public class Price implements ItemCharacteristic {
    private double value;

    public Price(double value) {
        this.value = value;
    }

    public double get() {
        return value;
    }

    public void add(Price price) {
        value += price.get();
    }

    public void add(Price price, Amount howMany) {
        Amount currentAmount = new Amount(1);
        while(currentAmount.get() <= howMany.get()) {
            // TODO implement comparator to Amount
            // TODO implement recursion method
            add(price);
            currentAmount.increase();
        }
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Price price = (Price) o;

        return Double.compare(price.value, value) == 0;
    }

    @Override
    public int hashCode() {
        long temp = Double.doubleToLongBits(value);

        return (int) (temp ^ (temp >>> 32));
    }
}
