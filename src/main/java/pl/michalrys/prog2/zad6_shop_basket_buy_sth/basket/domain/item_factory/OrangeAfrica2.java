package pl.michalrys.prog2.zad6_shop_basket_buy_sth.basket.domain.item_factory;

import pl.michalrys.prog2.zad6_shop_basket_buy_sth.basket.domain.item_characteristics.ItemCharacteristic;
import pl.michalrys.prog2.zad6_shop_basket_buy_sth.basket.domain.item_characteristics.Price;

class OrangeAfrica2 implements ItemToBuy {
    final private ItemCharacteristic itemToBuyType;
    final private ItemCharacteristic name;
    final private Price price;

    public OrangeAfrica2(ItemCharacteristic itemToBuyType, ItemCharacteristic name, Price price) {
        this.itemToBuyType = itemToBuyType;
        this.name = name;
        this.price = price;
    }

    @Override
    public ItemCharacteristic getType() {
        return itemToBuyType;
    }

    @Override
    public ItemCharacteristic getName() {
        return name;
    }

    @Override
    public Price getPrice() {
        return price;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OrangeAfrica2 that = (OrangeAfrica2) o;

        if (itemToBuyType != that.itemToBuyType) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        return price != null ? price.equals(that.price) : that.price == null;
    }

    @Override
    public int hashCode() {
        int result = itemToBuyType != null ? itemToBuyType.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        return result;
    }
}
