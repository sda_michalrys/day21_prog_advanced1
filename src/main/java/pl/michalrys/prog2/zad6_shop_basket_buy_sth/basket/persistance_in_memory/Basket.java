package pl.michalrys.prog2.zad6_shop_basket_buy_sth.basket.persistance_in_memory;

import pl.michalrys.prog2.zad6_shop_basket_buy_sth.basket.domain.item_characteristics.Amount;
import pl.michalrys.prog2.zad6_shop_basket_buy_sth.basket.domain.item_factory.ItemToBuy;
import pl.michalrys.prog2.zad6_shop_basket_buy_sth.basket.domain.item_characteristics.Price;
import pl.michalrys.prog2.zad6_shop_basket_buy_sth.basket.exceptions.AmountOfItemsToRemoveIsGreaterThanAmountOfItemsInBasketException;
import pl.michalrys.prog2.zad6_shop_basket_buy_sth.basket.exceptions.ItemDoNotExistInBasketException;

import java.util.HashMap;
import java.util.Map;

class Basket {
    private Map<ItemToBuy, Amount> items;

    public Basket() {
        items = new HashMap<>();
    }

    public void add(ItemToBuy itemToBuy) {
        Amount amount;
        if (items.containsKey(itemToBuy)) {
            amount = items.get(itemToBuy);
            amount.increase();
            items.put(itemToBuy, amount);
            return;
        }
        amount = new Amount(1);
        items.put(itemToBuy, amount);
    }

//    public void add(ItemToBuy itemToBuy, int howMany) {
//        Amount amount;
//        if (items.containsKey(itemToBuy)) {
//            amount = items.get(itemToBuy);
//            amount.increase(howMany);
//            items.put(itemToBuy, amount);
//            return;
//        }
//        amount = new Amount(howMany);
//        items.put(itemToBuy, amount);
//    }

    public void remove(ItemToBuy itemToBuy) {
        if (!items.containsKey(itemToBuy)) {
            throw new ItemDoNotExistInBasketException(itemToBuy);
        }
        Amount singleItemAmount = new Amount(1);
        if (items.get(itemToBuy).equals(singleItemAmount)) {
            items.remove(itemToBuy);
        } else {
            Amount amount = items.get(itemToBuy);
            amount.decrease();
            items.put(itemToBuy, amount);
        }
    }

    public void remove(ItemToBuy itemToBuy, int howMany) {
        if (!items.containsKey(itemToBuy)) {
            throw new ItemDoNotExistInBasketException(itemToBuy);
        }
        Amount howManyAmount = new Amount(howMany);
        if (items.get(itemToBuy).equals(howManyAmount)) {
            items.remove(itemToBuy);
        } else if (howManyAmount.get() > items.get(itemToBuy).get()) {
            throw new AmountOfItemsToRemoveIsGreaterThanAmountOfItemsInBasketException(howManyAmount);
        } else {
            Amount amount = items.get(itemToBuy);
            amount.decrease(howMany);
            items.put(itemToBuy, amount);
        }
    }

    public String get() {
        StringBuilder itemsList = new StringBuilder();
        itemsList.append("List of items in basket: (item : amount x unit price)\n");

        for (ItemToBuy itemToBuy : items.keySet()) {
            itemsList.append(itemToBuy.getName().toString() + " : ");
            itemsList.append(items.get(itemToBuy).get() + " x ");
            itemsList.append(itemToBuy.getPrice().get() + "\n");
        }
        itemsList.replace(itemsList.length() - 1, itemsList.length(), "");
        return itemsList.toString();
    }

    public String getPrice() {
        StringBuilder totalPriceInformation = new StringBuilder();
        Price totalPrice = new Price(0);
        Amount howMany;
        for (ItemToBuy itemToBuy : items.keySet()) {
            howMany = items.get(itemToBuy);
            totalPrice.add(itemToBuy.getPrice(), howMany);
        }
        totalPriceInformation.append("Total price is: " + totalPrice.get());
        return totalPriceInformation.toString();
    }
}
