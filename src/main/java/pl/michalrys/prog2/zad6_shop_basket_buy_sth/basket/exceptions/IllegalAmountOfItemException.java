package pl.michalrys.prog2.zad6_shop_basket_buy_sth.basket.exceptions;

public class IllegalAmountOfItemException extends RuntimeException {
    public IllegalAmountOfItemException(int howMany) {
        super("Given amount must be integer value greaten than 0. (Given value : " + howMany + ")");
    }
}
