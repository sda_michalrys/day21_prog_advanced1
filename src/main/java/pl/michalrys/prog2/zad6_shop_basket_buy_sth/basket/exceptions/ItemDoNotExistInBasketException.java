package pl.michalrys.prog2.zad6_shop_basket_buy_sth.basket.exceptions;

import pl.michalrys.prog2.zad6_shop_basket_buy_sth.basket.domain.item_factory.ItemToBuy;

public class ItemDoNotExistInBasketException extends RuntimeException {
    public ItemDoNotExistInBasketException(ItemToBuy itemToBuy) {
        super("Given item does not exist in basket - can not be deleted: " + itemToBuy);
    }
}
