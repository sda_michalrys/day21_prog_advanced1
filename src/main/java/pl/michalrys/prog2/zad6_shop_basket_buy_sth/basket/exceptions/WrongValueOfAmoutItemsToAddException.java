package pl.michalrys.prog2.zad6_shop_basket_buy_sth.basket.exceptions;

public class WrongValueOfAmoutItemsToAddException extends RuntimeException {
    public WrongValueOfAmoutItemsToAddException(int howMany) {
        super("Given amount of item : " + howMany + " is not acceptable.");
    }
}
