package pl.michalrys.prog2.zad6_shop_basket_buy_sth.basket.domain.item_factory;

import pl.michalrys.prog2.zad6_shop_basket_buy_sth.basket.domain.item_characteristics.ItemCharacteristic;
import pl.michalrys.prog2.zad6_shop_basket_buy_sth.basket.domain.item_characteristics.Price;

public interface ItemToBuy {
    ItemCharacteristic getType();

    ItemCharacteristic getName();

    Price getPrice();
}
