package pl.michalrys.prog2.zad6_shop_basket_buy_sth.basket.persistance_in_memory;

import java.util.HashSet;
import java.util.Set;

public class ProductsController {
    private Products products;

    public ProductsController() {
        this.products = new Products();
    }

    public void add(String productName) {
        // TODO validation
        products.add(productName);
    }

    public Set<String> getListOfProducts() {
        return products.get();
    }
}
