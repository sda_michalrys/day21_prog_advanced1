package pl.michalrys.prog2.zad6_shop_basket_buy_sth.basket.exceptions;

import pl.michalrys.prog2.zad6_shop_basket_buy_sth.basket.domain.item_characteristics.Amount;

public class AmountOfItemsToRemoveIsGreaterThanAmountOfItemsInBasketException extends RuntimeException {
    public AmountOfItemsToRemoveIsGreaterThanAmountOfItemsInBasketException(Amount amount) {
        super("Given amount of items to remove is greater than amount of items in basket : " + amount.get());
    }
}
