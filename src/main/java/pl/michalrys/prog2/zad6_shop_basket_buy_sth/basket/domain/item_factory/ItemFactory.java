package pl.michalrys.prog2.zad6_shop_basket_buy_sth.basket.domain.item_factory;

public interface ItemFactory {
    ItemToBuy create(String itemToAdd);
}
