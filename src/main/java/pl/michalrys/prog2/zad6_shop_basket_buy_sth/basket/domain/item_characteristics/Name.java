package pl.michalrys.prog2.zad6_shop_basket_buy_sth.basket.domain.item_characteristics;

public class Name implements ItemCharacteristic {
    final private String value;

    public Name(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Name name = (Name) o;

        return value != null ? value.equals(name.value) : name.value == null;
    }

    @Override
    public int hashCode() {
        return value != null ? value.hashCode() : 0;
    }

    @Override
    public String toString() {
        return value;
    }
}
