package pl.michalrys.prog2.zad6_shop_basket_buy_sth.basket.domain.item_characteristics;

import pl.michalrys.prog2.zad6_shop_basket_buy_sth.basket.domain.item_characteristics.ItemCharacteristic;

public enum ItemToBuyType implements ItemCharacteristic {
    MILK,
    TEA,
    ORANGE;
}
