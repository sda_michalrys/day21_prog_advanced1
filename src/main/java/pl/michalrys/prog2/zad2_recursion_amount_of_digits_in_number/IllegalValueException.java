package pl.michalrys.prog2.zad2_recursion_amount_of_digits_in_number;

public class IllegalValueException extends RuntimeException {
    public IllegalValueException(int decimal) {
        super("Given value is out of allowable scope <0, 1020> : " + decimal);
    }
}
