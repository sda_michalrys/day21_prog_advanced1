package pl.michalrys.prog2.zad2_recursion_amount_of_digits_in_number;

public class SumDigitsFromNaturalNumber {
    private static final int MIN_ALLOWABLE_VALUE = 0;
    private static final int MAX_ALLOWABLE_VALUE = 1024;
    private final int number;

    public SumDigitsFromNaturalNumber(int number) {
        if (number < MIN_ALLOWABLE_VALUE || number > MAX_ALLOWABLE_VALUE) {
            throw new IllegalValueException(number);
        }
        this.number = number;
    }

    // simplified
    public int getSumSimplyCode(int currentResultVal, int currentDecimalVal) {
        if (currentDecimalVal / 10 == 0) {
            return currentResultVal + currentDecimalVal % 10;
        } else {
            return getSumSimplyCode(currentResultVal + currentDecimalVal % 10, currentDecimalVal / 10);
        }
    }

    // more complex code
    public int getSum() {
        return sumOf(0, number);
    }

    private int sumOf(int currentResult, int currentDecimalValue) {
        currentResult += currentDecimalValue % 10;

        if (currentDecimalValue / 10 == 0) {
            return currentResult;
        } else {
            return sumOf(currentResult, currentDecimalValue / 10);
        }
    }

    public int getNumber() {
        return number;
    }
}
