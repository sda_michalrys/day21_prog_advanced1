package pl.michalrys.prog2.zad7_currency_exchange2;

import pl.michalrys.prog2.zad7_currency_exchange2.currency_exchange.persistance_in_memory.ExchangeOfficeController;

public class Client {
    public static void main(String[] args) {
        ExchangeOfficeController exchangeOfficeController = new ExchangeOfficeController();

        // firsty currency rates should be set up - admin login and password is needed
        exchangeOfficeController.setCurrencySellRate("Admin123", "Admin123", "EUR_FOR_PLN", 4.40);
        exchangeOfficeController.setCurrencyBuyRate("Admin123", "Admin123", "EUR_FOR_PLN", 4.30);

        exchangeOfficeController.setCurrencySellRate("Admin123", "Admin123", "PLN_FOR_EUR", 4.00);
        exchangeOfficeController.setCurrencyBuyRate("Admin123", "Admin123", "PLN_FOR_EUR", 4.50);

        exchangeOfficeController.setCurrencySellRate("Admin123", "Admin123", "USD_FOR_PLN", 3.30);
        exchangeOfficeController.setCurrencyBuyRate("Admin123", "Admin123", "USD_FOR_PLN", 3.10);

        exchangeOfficeController.setCurrencySellRate("Admin123", "Admin123", "EUR_FOR_USD", 3.30);
        exchangeOfficeController.setCurrencySpreadRate("Admin123", "Admin123", "EUR_FOR_USD", 0.20);


        // here comes Client and exchange some money
        double exchangeEuroForPln1 = exchangeOfficeController.sellCurrency("EUR_FOR_PLN", 100.0);
        double exchangeEuroForPln2 = exchangeOfficeController.buyCurrency("EUR_FOR_PLN", 100.0);

        double exchangeEuroForPln3 = exchangeOfficeController.sellCurrency("USD_FOR_PLN", 100.0);
        double exchangeEuroForPln4 = exchangeOfficeController.buyCurrency("USD_FOR_PLN", 100.0);

        // results of these actions
        System.out.println(exchangeEuroForPln1);
        System.out.println(exchangeEuroForPln2);
        System.out.println(exchangeEuroForPln3);
        System.out.println(exchangeEuroForPln4);
    }
}
