package pl.michalrys.prog2.zad7_currency_exchange2.currency_exchange.exceptions;

public class WrongAuthorizationDataNoPermissionForChangesException extends RuntimeException {
    public WrongAuthorizationDataNoPermissionForChangesException() {
        super("For this authorization data you have no permissions to add any change in office exchange.");
    }
}
