package pl.michalrys.prog2.zad7_currency_exchange2.currency_exchange.persistance_in_memory;

import pl.michalrys.prog2.zad7_currency_exchange2.currency_exchange.domain.currency.Currency;
import pl.michalrys.prog2.zad7_currency_exchange2.currency_exchange.domain.currency.CurrencyCreateFactory;
import pl.michalrys.prog2.zad7_currency_exchange2.currency_exchange.domain.currency.CurrencyExchangeType;
import pl.michalrys.prog2.zad7_currency_exchange2.currency_exchange.domain.user.UserData;
import pl.michalrys.prog2.zad7_currency_exchange2.currency_exchange.domain.user.UserLoginCreateFactory;
import pl.michalrys.prog2.zad7_currency_exchange2.currency_exchange.domain.user.UserPasswordCreateFactory;
import pl.michalrys.prog2.zad7_currency_exchange2.currency_exchange.exceptions.WrongAuthorizationDataNoPermissionForChangesException;
import pl.michalrys.prog2.zad7_currency_exchange2.currency_exchange.exceptions.WrongCurrencyDoesNotExistsInExchangeOfficeException;

import java.util.HashMap;
import java.util.Map;

class ExchangeOffice {
    private final static String ADMIN_LOGIN = "Admin123";
    private final static String ADMIN_PASSWORD = "Admin123";
    private UserData admin;
    private UserData admin_password;
    private Map<CurrencyExchangeType, Currency> currencies;
    private final UserLoginCreateFactory loginFactory;
    private final UserPasswordCreateFactory passwordFactory;
    private final CurrencyCreateFactory currencyFactory;

    public ExchangeOffice() {
        // factories
        loginFactory = new UserLoginCreateFactory();
        passwordFactory = new UserPasswordCreateFactory();
        currencyFactory = new CurrencyCreateFactory();
        // rest of fields
        admin = loginFactory.create(ADMIN_LOGIN);
        admin_password = passwordFactory.create(ADMIN_PASSWORD);
        currencies = new HashMap<>();
    }

    void setCurrencySellRate(UserData login, UserData password,
                             CurrencyExchangeType currencyExchangeType, double sellRate) {

        if (!login.equals(admin) || !password.equals(admin_password)) {
            throw new WrongAuthorizationDataNoPermissionForChangesException();
        }

        Currency currencyToAdd;

        if (currencies.containsKey(currencyExchangeType)) {
            currencyToAdd = currencies.get(currencyExchangeType);
        } else {
            currencyToAdd = currencyFactory.create(currencyExchangeType);
        }

        currencyToAdd.setSellRate(sellRate);
        currencies.put(currencyExchangeType, currencyToAdd);
    }

    public void setCurrencyBuyRate(UserData login, UserData password,
                                   CurrencyExchangeType currencyExchangeType, double buyRate) {

        if (!login.equals(admin) || !password.equals(admin_password)) {
            throw new WrongAuthorizationDataNoPermissionForChangesException();
        }

        Currency currencyToAdd;

        if (currencies.containsKey(currencyExchangeType)) {
            currencyToAdd = currencies.get(currencyExchangeType);
        } else {
            currencyToAdd = currencyFactory.create(currencyExchangeType);
        }
        currencyToAdd.setBuyRate(buyRate);
        currencies.put(currencyExchangeType, currencyToAdd);

    }

    public double sellCurrency(CurrencyExchangeType currencyExchangeType, double amountOfMoney) {
        if (!currencies.containsKey(currencyExchangeType)) {
            throw new WrongCurrencyDoesNotExistsInExchangeOfficeException(currencyExchangeType);
        }
        Currency currencyToSell = currencies.get(currencyExchangeType);
        return currencyToSell.sell(amountOfMoney);
    }

    public double buyCurrency(CurrencyExchangeType currencyExchangeType, double amountOfMoney) {
        if (!currencies.containsKey(currencyExchangeType)) {
            throw new WrongCurrencyDoesNotExistsInExchangeOfficeException(currencyExchangeType);
        }
        Currency currencyToSell = currencies.get(currencyExchangeType);
        return currencyToSell.buy(amountOfMoney);
    }
}
