package pl.michalrys.prog2.zad7_currency_exchange2.currency_exchange.exceptions;

public class WrongCurrencyExchangeException extends RuntimeException {
    public WrongCurrencyExchangeException(String currencyExchangeFor) {
        super("Given input data is wrong: " + currencyExchangeFor + ". This should be something like: EUR_FOR_PLN");
    }
}
