package pl.michalrys.prog2.zad7_currency_exchange2.currency_exchange.domain.currency;

class CurrencyEUR_for_USD implements Currency {
    private double sellRate;
    private double buyRate;
    private double spread;
    private CurrencyExchangeType currencyExchangeType;

    public CurrencyEUR_for_USD() {
        sellRate = 0;
        buyRate = 0;
        spread = 0;
        currencyExchangeType = CurrencyExchangeType.EUR_FOR_USD;
    }

    @Override
    public void setSellRate(double sellRate) {
        this.sellRate = sellRate;
        if (spread != 0) {
            buyRate = sellRate - spread;
        }
        if (buyRate != 0) {
            spread = sellRate - buyRate;
        }
    }

    @Override
    public void setBuyRate(double buyRate) {
        this.buyRate = buyRate;
        if (spread != 0) {
            sellRate = buyRate + spread;
        }
        if (sellRate != 0) {
            spread = sellRate - buyRate;
        }
    }

    @Override
    public void setSpread(double spreadRate) {
        spread = spreadRate;
        if (sellRate != 0) {
            buyRate = sellRate - spread;
        }
        if (buyRate != 0) {
            sellRate = buyRate + spread;
        }
    }

    @Override
    public double sell(double amountOfMoney) {
        return amountOfMoney * sellRate;
    }

    @Override
    public double buy(double amountOfMoney) {
        return amountOfMoney * buyRate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CurrencyEUR_for_USD that = (CurrencyEUR_for_USD) o;

        if (Double.compare(that.sellRate, sellRate) != 0) return false;
        if (Double.compare(that.buyRate, buyRate) != 0) return false;
        if (Double.compare(that.spread, spread) != 0) return false;
        return currencyExchangeType == that.currencyExchangeType;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(sellRate);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(buyRate);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(spread);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (currencyExchangeType != null ? currencyExchangeType.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return currencyExchangeType.toString() + " sell: " + sellRate + " spread: " + spread;
    }
}
