package pl.michalrys.prog2.zad7_currency_exchange2.currency_exchange.exceptions;

public class WrongGivenLoginException extends RuntimeException {
    public WrongGivenLoginException(String login) {
        super("Given login is wrong : " + login);
    }
}
