package pl.michalrys.prog2.zad7_currency_exchange2.currency_exchange.domain.currency;

public enum CurrencyExchangeType {
    EUR_FOR_PLN,
    PLN_FOR_EUR,

    USD_FOR_PLN,
    PLN_FOR_USD,

    USD_FOR_EUR,
    EUR_FOR_USD;
}
