package pl.michalrys.prog2.zad7_currency_exchange2.currency_exchange.domain.user;

public class UserLoginCreateFactory implements UserLoginFactory {
    @Override
    public UserData create(String userLogin) {
        return new UserLogin(userLogin);
    }
}
