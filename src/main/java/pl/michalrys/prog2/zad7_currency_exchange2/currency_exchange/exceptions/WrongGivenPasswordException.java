package pl.michalrys.prog2.zad7_currency_exchange2.currency_exchange.exceptions;

public class WrongGivenPasswordException extends RuntimeException {
    public WrongGivenPasswordException(String password) {
        super("Given password is wrong ! Nothing can be done. Sorry. (" + password + ")");
    }
}
