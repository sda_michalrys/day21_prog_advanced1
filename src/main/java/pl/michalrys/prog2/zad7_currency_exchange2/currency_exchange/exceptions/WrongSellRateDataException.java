package pl.michalrys.prog2.zad7_currency_exchange2.currency_exchange.exceptions;

public class WrongSellRateDataException extends RuntimeException {
    public WrongSellRateDataException(double sellRate) {
        super("Given sell rate is wrong: " + sellRate);
    }
}
