package pl.michalrys.prog2.zad7_currency_exchange2.currency_exchange.persistance_in_memory;

import pl.michalrys.prog2.zad7_currency_exchange2.currency_exchange.domain.currency.CurrencyCreateFactory;
import pl.michalrys.prog2.zad7_currency_exchange2.currency_exchange.domain.currency.CurrencyExchangeType;
import pl.michalrys.prog2.zad7_currency_exchange2.currency_exchange.domain.user.UserData;
import pl.michalrys.prog2.zad7_currency_exchange2.currency_exchange.domain.user.UserLoginCreateFactory;
import pl.michalrys.prog2.zad7_currency_exchange2.currency_exchange.domain.user.UserPasswordCreateFactory;
import pl.michalrys.prog2.zad7_currency_exchange2.currency_exchange.exceptions.WrongCurrencyExchangeException;
import pl.michalrys.prog2.zad7_currency_exchange2.currency_exchange.exceptions.WrongGivenLoginException;
import pl.michalrys.prog2.zad7_currency_exchange2.currency_exchange.exceptions.WrongGivenPasswordException;
import pl.michalrys.prog2.zad7_currency_exchange2.currency_exchange.exceptions.WrongSellRateDataException;

public class ExchangeOfficeController {
    private UserLoginValidator userLoginValidator;
    private UserPasswordValidator userPasswordValidator;
    private CurrencyExchangeValidator currencyExchangeValidator;
    private UserGivenCurrencyRateValidator userGivenCurrencyRateValidator;
    private ExchangeOffice exchangeOffice;
    private final UserLoginCreateFactory loginFactory;
    private final UserPasswordCreateFactory passwordFactory;
    private final CurrencyCreateFactory currencyFactory;

    public ExchangeOfficeController() {
        userLoginValidator = new UserLoginValidator();
        userPasswordValidator = new UserPasswordValidator();
        currencyExchangeValidator = new CurrencyExchangeValidator();
        userGivenCurrencyRateValidator = new UserGivenCurrencyRateValidator();
        exchangeOffice = new ExchangeOffice();
        // factories
        loginFactory = new UserLoginCreateFactory();
        passwordFactory = new UserPasswordCreateFactory();
        currencyFactory = new CurrencyCreateFactory();
    }

    public ExchangeOfficeController(ExchangeOffice existingExchangeOffice) {
        // use this constructor, if you want to use some other existing exchange office
        userLoginValidator = new UserLoginValidator();
        userPasswordValidator = new UserPasswordValidator();
        currencyExchangeValidator = new CurrencyExchangeValidator();
        userGivenCurrencyRateValidator = new UserGivenCurrencyRateValidator();
        exchangeOffice = existingExchangeOffice;
        // factories
        loginFactory = new UserLoginCreateFactory();
        passwordFactory = new UserPasswordCreateFactory();
        currencyFactory = new CurrencyCreateFactory();
    }

    public void setCurrencySellRate(String login, String password, String currencyExchangeFor, double sellRate) {
        if (!userLoginValidator.isValid(login)) {
            throw new WrongGivenLoginException(login);
        }
        if (!userPasswordValidator.isValid(password)) {
            throw new WrongGivenPasswordException(password);
        }
        if (!currencyExchangeValidator.isCorrect(currencyExchangeFor)) {
            throw new WrongCurrencyExchangeException(currencyExchangeFor);
        }

        if (!userGivenCurrencyRateValidator.isCorrect(sellRate)) {
            throw new WrongSellRateDataException(sellRate);
        }

        UserData loginWithPermissions = loginFactory.create(login);
        UserData passwordForGivenLogin = passwordFactory.create(password);
        CurrencyExchangeType currencyExchangeType = CurrencyExchangeType.valueOf(currencyExchangeFor);

        exchangeOffice.setCurrencySellRate(loginWithPermissions, passwordForGivenLogin, currencyExchangeType, sellRate);
    }

    public void setCurrencyBuyRate(String login, String password, String currencyExchangeFor, double buyRate) {
        if (!userLoginValidator.isValid(login)) {
            throw new WrongGivenLoginException(login);
        }
        if (!userPasswordValidator.isValid(password)) {
            throw new WrongGivenPasswordException(password);
        }
        if (!currencyExchangeValidator.isCorrect(currencyExchangeFor)) {
            throw new WrongCurrencyExchangeException(currencyExchangeFor);
        }

        if (!userGivenCurrencyRateValidator.isCorrect(buyRate)) {
            throw new WrongSellRateDataException(buyRate);
        }

        UserData loginWithPermissions = loginFactory.create(login);
        UserData passwordForGivenLogin = passwordFactory.create(password);
        CurrencyExchangeType currencyExchangeType = CurrencyExchangeType.valueOf(currencyExchangeFor);

        exchangeOffice.setCurrencyBuyRate(loginWithPermissions, passwordForGivenLogin, currencyExchangeType, buyRate);
    }

    public void setCurrencySpreadRate(String login, String password, String currencyExchangeFor, double spreadRate) {
        // TODO implement in future
    }

    public double sellCurrency(String currencyExchangeFor, double amountOfMoney) {
        if (!currencyExchangeValidator.isCorrect(currencyExchangeFor)) {
            throw new WrongCurrencyExchangeException(currencyExchangeFor);
        }
        CurrencyExchangeType currencyExchangeType = CurrencyExchangeType.valueOf(currencyExchangeFor);

        return exchangeOffice.sellCurrency(currencyExchangeType, amountOfMoney);
    }

    public double buyCurrency(String currencyExchangeFor, double amountOfMoney) {
        if (!currencyExchangeValidator.isCorrect(currencyExchangeFor)) {
            throw new WrongCurrencyExchangeException(currencyExchangeFor);
        }
        CurrencyExchangeType currencyExchangeType = CurrencyExchangeType.valueOf(currencyExchangeFor);

        return exchangeOffice.buyCurrency(currencyExchangeType, amountOfMoney);
    }
}
