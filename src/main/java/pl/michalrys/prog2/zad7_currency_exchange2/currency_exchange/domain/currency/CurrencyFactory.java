package pl.michalrys.prog2.zad7_currency_exchange2.currency_exchange.domain.currency;

public interface CurrencyFactory {
    Currency create(CurrencyExchangeType currencyExchangeType);
}
