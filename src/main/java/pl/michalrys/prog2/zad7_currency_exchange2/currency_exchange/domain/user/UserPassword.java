package pl.michalrys.prog2.zad7_currency_exchange2.currency_exchange.domain.user;

class UserPassword implements UserData {
    private final String password;

    public UserPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserPassword userPassword1 = (UserPassword) o;

        return password != null ? password.equals(userPassword1.password) : userPassword1.password == null;
    }

    @Override
    public int hashCode() {
        return password != null ? password.hashCode() : 0;
    }

    @Override
    public String toString() {
        return password;
    }
}
