package pl.michalrys.prog2.zad7_currency_exchange2.currency_exchange.exceptions;

import pl.michalrys.prog2.zad7_currency_exchange2.currency_exchange.domain.currency.CurrencyExchangeType;

public class WrongCurrencyDoesNotExistsInExchangeOfficeException extends RuntimeException {
    public WrongCurrencyDoesNotExistsInExchangeOfficeException(CurrencyExchangeType currencyExchangeType) {
        super("Currency does not exist in currency office: " + currencyExchangeType.toString());
    }
}
