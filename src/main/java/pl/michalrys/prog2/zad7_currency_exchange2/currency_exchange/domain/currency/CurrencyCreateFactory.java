package pl.michalrys.prog2.zad7_currency_exchange2.currency_exchange.domain.currency;

public class CurrencyCreateFactory implements CurrencyFactory {
    @Override
    public Currency create(CurrencyExchangeType currencyExchangeType) {
        Currency currency = null;

        switch (currencyExchangeType) {
            case EUR_FOR_PLN:
                return new CurrencyEUR_for_PLN();
            case PLN_FOR_EUR:
                return new CurrencyPLN_for_EUR();
            case EUR_FOR_USD:
                return new CurrencyEUR_for_USD();
            case USD_FOR_EUR:
                return new CurrencyUSD_for_EUR();
            case PLN_FOR_USD:
                return new CurrencyEUR_for_USD();
            case USD_FOR_PLN:
                return new CurrencyUSD_for_PLN();
        }
        return currency;
    }
}
