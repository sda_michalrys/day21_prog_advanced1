package pl.michalrys.prog2.zad7_currency_exchange2.currency_exchange.persistance_in_memory;

class UserGivenCurrencyRateValidator {
    public boolean isCorrect(double sellRate) {
        //TODO improve later
        return sellRate > 0 && sellRate < 10;
    }
}
