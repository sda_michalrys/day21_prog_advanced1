package pl.michalrys.prog2.zad7_currency_exchange2.currency_exchange.exceptions;

public class WrongDataOfficeExchange extends RuntimeException {
    public WrongDataOfficeExchange(String error_in_setCurrencySellRate) {
        super(error_in_setCurrencySellRate);
    }
}
