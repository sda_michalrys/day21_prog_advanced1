package pl.michalrys.prog2.zad7_currency_exchange2.currency_exchange.domain.user;

public class UserPasswordCreateFactory implements UserPasswordFactory {
    @Override
    public UserData create(String password) {
        return new UserPassword(password);
    }
}
