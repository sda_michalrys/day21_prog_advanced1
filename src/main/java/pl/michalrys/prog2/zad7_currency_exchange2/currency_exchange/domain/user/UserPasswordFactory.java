package pl.michalrys.prog2.zad7_currency_exchange2.currency_exchange.domain.user;

public interface UserPasswordFactory {
    UserData create(String password);
}
