package pl.michalrys.prog2.zad7_currency_exchange2.currency_exchange.domain.currency;

public interface Currency {
    void setSellRate(double sellRate);
    void setBuyRate(double buyRate);
    void setSpread(double spreadRate);
    double sell(double amountOfMoney);
    double buy(double amountOfMoney);
}
