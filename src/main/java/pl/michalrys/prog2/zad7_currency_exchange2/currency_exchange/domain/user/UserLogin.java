package pl.michalrys.prog2.zad7_currency_exchange2.currency_exchange.domain.user;

class UserLogin implements UserData {
    private final String login;

    public UserLogin(String login) {
        this.login = login;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserLogin userLogin1 = (UserLogin) o;

        return login != null ? login.equals(userLogin1.login) : userLogin1.login == null;
    }

    @Override
    public int hashCode() {
        return login != null ? login.hashCode() : 0;
    }

    @Override
    public String toString() {
        return login;
    }
}
