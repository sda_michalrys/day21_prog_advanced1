package pl.michalrys.prog2.zad1_recursion_decimal2binary;

public class NegativeDecimalValueException extends RuntimeException {
    public NegativeDecimalValueException(int decimalValue) {
        super("Negative value is not allowed : " + decimalValue);
    }
}
