package pl.michalrys.prog2.zad1_recursion_decimal2binary;

public class DecimalToBinaryConversion {

    public String getBinaryValue(int decimal) {
        if( decimal < 0) {
            throw new NegativeDecimalValueException(decimal);
        }
        StringBuilder binaryValue = new StringBuilder();
        convert(binaryValue, decimal);
        return binaryValue.reverse().toString();
    }

    private void convert(StringBuilder currentBinaryValue, int currentDecimal) {
        int result = currentDecimal % 2;
        currentBinaryValue.append(result);

        if (currentDecimal / 2 == 0) {
            return;
        }
        convert(currentBinaryValue, currentDecimal/2);
    }
}
