package pl.michalrys.prog2.zad3_enum_world_directions.directions;

import java.util.Scanner;

public class UserControler {
    private Scanner scanner;

    public UserControler() {
        scanner = new Scanner(System.in);
    }

    public void printDirections() {
        System.out.println("Allowable directions are: " + Direction.getAll());
    }

    public void askDirection() {
        System.out.println("Give direction: ");
        String userDirection = scanner.nextLine();
        Direction direction = Direction.conversion(userDirection);
        System.out.println("We are going on : " + direction + " (" + direction.getDescription() + ")");
    }
}
