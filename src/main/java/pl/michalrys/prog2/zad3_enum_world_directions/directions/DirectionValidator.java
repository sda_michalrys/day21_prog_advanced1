package pl.michalrys.prog2.zad3_enum_world_directions.directions;

class DirectionValidator {

    boolean isCorrect(String userDirection) {
        for (Direction direction : Direction.values()) {
            if (userDirection.equals(direction.toString())) {
                return true;
            }
        }
        return false;
    }
}
