package pl.michalrys.prog2.zad3_enum_world_directions.directions;

public class WrongGivenDirectionException extends RuntimeException {
    public WrongGivenDirectionException(String userDirection) {
        super("Wrong given direction : " + userDirection + ". It must be one of them: ... .");
    }
}
