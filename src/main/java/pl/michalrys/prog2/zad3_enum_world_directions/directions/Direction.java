package pl.michalrys.prog2.zad3_enum_world_directions.directions;

enum Direction {
    NORTH("Polnoc"),
    SOUTH("Poludnie"),
    EAST("Wschod"),
    WEST("Zachod");

    String description;

    Direction(String description) {
        this.description = description;
    }

    public static Direction conversion(String userDirection) {
        DirectionValidator directionValidator = new DirectionValidator();
        userDirection = userDirection.toUpperCase();

        if (directionValidator.isCorrect(userDirection)) {
            return Direction.valueOf(userDirection);
        }

        // additional bonus input
        switch (userDirection) {
            case "N":
                return Direction.NORTH;
            case "S":
                return Direction.SOUTH;
            case "E":
                return Direction.EAST;
            case "W":
                return Direction.WEST;
            case "n":
                return Direction.NORTH;
            case "s":
                return Direction.SOUTH;
            case "e":
                return Direction.EAST;
            case "w":
                return Direction.WEST;
        }
        throw new WrongGivenDirectionException(userDirection);
    }

    static String getAll() {
        StringBuilder directions = new StringBuilder();

        for (Direction direction : Direction.values()) {
            directions.append(direction.toString());
            directions.append(", ");
        }
        directions.replace(directions.length() - 2, directions.length(), "");

        return directions.toString();
    }

    String getDescription() {
        return description;
    }
}
