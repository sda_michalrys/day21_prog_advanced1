package pl.michalrys.prog2.zad3_enum_world_directions;
import pl.michalrys.prog2.zad3_enum_world_directions.directions.UserControler;

public class Client {
    public static void main(String[] args) {
        UserControler userControler = new UserControler();

        userControler.printDirections();
        userControler.askDirection();
    }
}
