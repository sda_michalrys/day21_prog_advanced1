package pl.michalrys.prog2.zad7_currency_exchange;

import pl.michalrys.prog2.zad7_currency_exchange.currency_exchange.CurrencyExchangeController;

public class Client {
    public static void main(String[] args) {
        CurrencyExchangeController currencyExchangeController = new CurrencyExchangeController();

        double euro1 = currencyExchangeController.change("PLN", "EUR", 100);
        double pln1 = currencyExchangeController.change("EUR", "PLN", 100);
        double usd1 = currencyExchangeController.change("PLN", "USD", 100);
        double usd2 = currencyExchangeController.change("EUR", "USD", 100);

        System.out.println(euro1);
        System.out.println(pln1);
        System.out.println(usd1);
        System.out.println(usd2);
    }
}
