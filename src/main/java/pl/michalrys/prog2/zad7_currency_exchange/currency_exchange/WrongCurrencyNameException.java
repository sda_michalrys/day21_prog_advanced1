package pl.michalrys.prog2.zad7_currency_exchange.currency_exchange;

public class WrongCurrencyNameException extends RuntimeException {
    public WrongCurrencyNameException(String currencyFromUI) {
        super("Given currency does not exist in database : " + currencyFromUI);
    }
}
