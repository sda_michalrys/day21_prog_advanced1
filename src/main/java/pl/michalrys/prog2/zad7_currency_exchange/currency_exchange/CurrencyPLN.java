package pl.michalrys.prog2.zad7_currency_exchange.currency_exchange;

class CurrencyPLN implements Currency {
    private double value;
    private final CurrencyExchange currencyExchange;
    private final CurrencyType currencyType;

    CurrencyPLN(double value, CurrencyExchange currencyExchange) {
        this.value = value;
        this.currencyExchange = currencyExchange;
        this.currencyType = CurrencyType.PLN;
    }

    public CurrencyPLN(CurrencyExchange currencyExchange) {
        value = 0;
        this.currencyExchange = currencyExchange;
        this.currencyType = CurrencyType.PLN;
    }

    @Override
    public void exchangeFrom(Currency currencyFrom) {
        String temp = "";
        temp += currencyFrom.getType().toString();
        temp += "2";
        temp = getType().toString();

        value = currencyFrom.get() * currencyFrom.getCurrencyExchange().getExchangeRate(CurrencyRates.valueOf(temp));
    }

    @Override
    public double get() {
        return value;
    }

    @Override
    public CurrencyExchange getCurrencyExchange() {
        return currencyExchange;
    }

    @Override
    public CurrencyType getType() {
        return currencyType;
    }
}
