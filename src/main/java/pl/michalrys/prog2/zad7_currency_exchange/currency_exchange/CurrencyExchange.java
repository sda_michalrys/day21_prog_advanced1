package pl.michalrys.prog2.zad7_currency_exchange.currency_exchange;

import java.util.HashMap;
import java.util.Map;

class CurrencyExchange {
    private Map<CurrencyRates, Double> exchangeRates;

    CurrencyExchange() {
        this.exchangeRates = new HashMap<>();
    }

    void udpateExchangeRate(CurrencyRates currencyRates, Double rate) {
        exchangeRates.put(currencyRates, rate);
    }

    double getExchangeRate(CurrencyRates currencyRates) {
        return exchangeRates.get(currencyRates);
    }
}
