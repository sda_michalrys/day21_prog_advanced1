package pl.michalrys.prog2.zad7_currency_exchange.currency_exchange;

class CurrencyEUR implements Currency {
    private double value;
    private final CurrencyExchange currencyExchange;
    private final CurrencyType currencyType;

    CurrencyEUR(double value, CurrencyExchange currencyExchange) {
        this.value = value;
        this.currencyExchange = currencyExchange;
        this.currencyType = CurrencyType.EUR;
    }

    public CurrencyEUR(CurrencyExchange currencyExchange) {
        value = 0;
        this.currencyExchange = currencyExchange;
        this.currencyType = CurrencyType.EUR;
    }

    @Override
    public void exchangeFrom(Currency currencyFrom) {

    }

    @Override
    public double get() {
        return value;
    }

    @Override
    public CurrencyExchange getCurrencyExchange() {
        return currencyExchange;
    }

    @Override
    public CurrencyType getType() {
        return currencyType;
    }
}
