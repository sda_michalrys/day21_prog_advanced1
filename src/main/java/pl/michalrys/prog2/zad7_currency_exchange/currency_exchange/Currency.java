package pl.michalrys.prog2.zad7_currency_exchange.currency_exchange;

interface Currency {
    void exchangeFrom(Currency currencyFrom);

    double get();

    CurrencyType getType();

    CurrencyExchange getCurrencyExchange();
}
