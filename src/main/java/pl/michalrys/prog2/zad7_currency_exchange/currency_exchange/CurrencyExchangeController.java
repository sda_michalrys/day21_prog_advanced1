package pl.michalrys.prog2.zad7_currency_exchange.currency_exchange;

public class CurrencyExchangeController {
    private final CurrencyValidator currencyValidator;
    private final CurrencyExchange currencyExchange;

    public CurrencyExchangeController() {
        this.currencyValidator = new CurrencyValidator();
        this.currencyExchange = new CurrencyExchange();

        //TODO update later
        currencyExchange.udpateExchangeRate(CurrencyRates.EUR2EUR, 1.0);
        currencyExchange.udpateExchangeRate(CurrencyRates.EUR2PLN, 4.40);
        currencyExchange.udpateExchangeRate(CurrencyRates.EUR2USD, 1.50);

        currencyExchange.udpateExchangeRate(CurrencyRates.PLN2EUR, 4.0);
        currencyExchange.udpateExchangeRate(CurrencyRates.PLN2PLN, 1.0);
        currencyExchange.udpateExchangeRate(CurrencyRates.PLN2USD, 3.00);

        currencyExchange.udpateExchangeRate(CurrencyRates.USD2EUR, 1.40);
        currencyExchange.udpateExchangeRate(CurrencyRates.USD2PLN, 3.50);
        currencyExchange.udpateExchangeRate(CurrencyRates.USD2USD, 1.00);
    }

    public double change(String currencyFromUI, String currencyToUI, double value) {
        if (!currencyValidator.isCorrect(currencyFromUI)) {
            throw new WrongCurrencyNameException(currencyFromUI);
        }
        if (!currencyValidator.isCorrect(currencyToUI)) {
            throw new WrongCurrencyNameException(currencyToUI);
        }

        CurrencyType currencyTypeFrom = CurrencyType.valueOf(currencyFromUI);
        CurrencyType currencyTypeTo = CurrencyType.valueOf(currencyToUI);
        Currency currencyFrom = null;
        Currency currencyTo = null;

        if (currencyTypeFrom.equals(CurrencyType.PLN)) {
            currencyFrom = new CurrencyPLN(value, currencyExchange);
        } else if (currencyTypeFrom.equals(CurrencyType.USD)) {
            currencyFrom = new CurrencyUSD(value, currencyExchange);
        } else if (currencyTypeFrom.equals(CurrencyType.EUR)) {
            currencyFrom = new CurrencyEUR(value, currencyExchange);
        }

        if (currencyTypeTo.equals(CurrencyType.PLN)) {
            currencyTo = new CurrencyPLN(currencyExchange);
        } else if (currencyTypeTo.equals(CurrencyType.USD)) {
            currencyTo = new CurrencyUSD(currencyExchange);
        } else if (currencyTypeTo.equals(CurrencyType.EUR)) {
            currencyTo = new CurrencyEUR(currencyExchange);
        }

        currencyTo.exchangeFrom(currencyFrom);

        return currencyTo.get();
    }
}
