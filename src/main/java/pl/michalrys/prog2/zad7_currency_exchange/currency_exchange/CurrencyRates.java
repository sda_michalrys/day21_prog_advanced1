package pl.michalrys.prog2.zad7_currency_exchange.currency_exchange;

enum CurrencyRates {
    PLN2EUR,
    PLN2USD,
    PLN2PLN,
    USD2PLN,
    USD2EUR,
    USD2USD,
    EUR2PLN,
    EUR2USD,
    EUR2EUR;
}
