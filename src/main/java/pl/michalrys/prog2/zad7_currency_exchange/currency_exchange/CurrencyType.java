package pl.michalrys.prog2.zad7_currency_exchange.currency_exchange;

enum CurrencyType {
    PLN,
    USD,
    EUR;
}
