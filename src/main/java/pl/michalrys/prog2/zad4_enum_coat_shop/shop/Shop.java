package pl.michalrys.prog2.zad4_enum_coat_shop.shop;

import java.util.ArrayList;
import java.util.List;

public class Shop {
    private List<ItemToSell> itemToSells;

    public Shop() {
        this.itemToSells = new ArrayList<>();
    }

    public void add(Coat coat) {
        itemToSells.add(coat);
    }

    public List<ItemToSell> get(Color color) {
        List<ItemToSell> coatsWithTheSameColor = new ArrayList<>();
        for (ItemToSell itemToSell : itemToSells) {
            if (itemToSell instanceof Coat) {
                if (((Coat) itemToSell).getColor() == color) {
                    coatsWithTheSameColor.add(itemToSell);
                }
            }
        }
        return coatsWithTheSameColor;
    }

    public List<ItemToSell> get(Fabric fabric) {
        List<ItemToSell> coatsWithTheSameFabric = new ArrayList<>();
        for (ItemToSell itemToSell : itemToSells) {
            if (((Coat) itemToSell).getFabric() == fabric) {
                coatsWithTheSameFabric.add(itemToSell);
            }
        }
        return coatsWithTheSameFabric;
    }
                // TODO przerobić coat na itemToSell
    public List<ItemToSell> get(Color color, Fabric fabric) {
        List<ItemToSell> coatsWithTheSameColorAndFabric = new ArrayList<>();
        List<ItemToSell> coatsWithTheSameColor = get(color);
        List<ItemToSell> coatsWithTheSameFabric = get(fabric);

        for (ItemToSell coat : coatsWithTheSameColor) {
            if (coatsWithTheSameFabric.contains(coat)) {
                coatsWithTheSameColorAndFabric.add(coat);
            }
        }

        return coatsWithTheSameColorAndFabric;
    }
}
