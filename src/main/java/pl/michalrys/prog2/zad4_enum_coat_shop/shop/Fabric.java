package pl.michalrys.prog2.zad4_enum_coat_shop.shop;

public enum Fabric {
    COTTON,
    POLYESTER,
    WOOL;
}
