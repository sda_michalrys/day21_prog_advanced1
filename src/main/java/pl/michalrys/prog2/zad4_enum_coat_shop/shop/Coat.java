package pl.michalrys.prog2.zad4_enum_coat_shop.shop;

class Coat implements ItemToSell {
    private final ItemType itemType;
    private final Color color;
    private final Fabric fabric;
    private final int size;

    Coat(Color color, Fabric fabric, int size) {
        this.color = color;
        this.fabric = fabric;
        this.size = size;
        itemType = ItemType.COAT;
    }

    ItemType getItemType() {
        return itemType;
    }

    int getSize() {
        return size;
    }

    Color getColor() {
        return color;
    }

    Fabric getFabric() {
        return fabric;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Coat coat = (Coat) o;

        if (size != coat.size) return false;
        if (itemType != coat.itemType) return false;
        if (color != coat.color) return false;
        return fabric == coat.fabric;
    }

    @Override
    public int hashCode() {
        int result = itemType != null ? itemType.hashCode() : 0;
        result = 31 * result + (color != null ? color.hashCode() : 0);
        result = 31 * result + (fabric != null ? fabric.hashCode() : 0);
        result = 31 * result + size;
        return result;
    }
}
