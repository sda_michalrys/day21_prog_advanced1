package pl.michalrys.prog2.zad4_enum_coat_shop;

import pl.michalrys.prog2.zad4_enum_coat_shop.shop.*;

public class Client {
    public static void main(String[] args) {
        ShopControler shopControler = new ShopControler();

        shopControler.add("Coat", "Red", "Wool", 10);
        shopControler.add("Coat", "Blue", "Cotton", 10);
        shopControler.add("Coat", "Red", "Polyester", 12);

        System.out.println(shopControler.get("Coat", "Red"));
        System.out.println(shopControler.get("Coat", "Wool"));
        //System.out.println(shopControler.get("Coat", "Red", "Wool"));

//        Shop basket = new Shop();
//
//        Coat coat1 = new Coat(Color.RED, Fabric.WOOL, 10);
//        Coat coat2 = new Coat(Color.BLUE, Fabric.COTTON, 10);
//        Coat coat3 = new Coat(Color.RED, Fabric.POLYESTER, 12);
//
//        basket.add(coat1);
//        basket.add(coat2);
//        basket.add(coat3);
//
//        System.out.println(basket.get(Color.RED));
//        System.out.println(basket.get(Fabric.WOOL));
//        System.out.println(basket.get(Color.RED, Fabric.WOOL));
    }
}
